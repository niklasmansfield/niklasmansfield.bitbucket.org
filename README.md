Patwic HTML-Övning
==================

Börja med att göra en *fork* av det här repositoryt.

Japp, forken är gjord och klonad och klar är den också. Nu ska vi se hur vi får in den här ändringen i repot som ligger på min dator.

Första gången löste jag problemet genom att köra kommandona
```shell
$ git fetch
$ git branch -r
$ git checkout master
$ git merge origin/master
```

Andra gången nöjde jag mig med
```shell
$ git pull
```

och det gav typ samma resultat tror jag. Nu ska jag testa **rebase** genom att köra
```shell
$ git pull --rebase
```